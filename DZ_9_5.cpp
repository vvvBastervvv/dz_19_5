﻿// DZ_9_5.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>


class Animal
{

public:
	Animal() {};
	virtual ~Animal() { std::cout << "Delete Animal\n"; };
	

	virtual void Voice()
	{
		std::cout << "Default\n";
	}

	
};

class Doge : public Animal
{
public:
	Doge() {};
	~Doge() { std::cout << "Delete Doge\n"; };


	void Voice() override
	{
		std::cout << "Woof\n";
	}
};

class Cat : public Animal
{
public:
	Cat() {};
	~Cat() { std::cout << "Delete Cat\n"; };

	void Voice() override
	{
		std::cout << "Meow\n";
	}
};

class Cow : public Animal
{
public:
	Cow() {};
	~Cow() { std::cout << "Delete Cow\n"; };

	void Voice() override
	{
		std::cout << "Muuuu\n";
	}
};

int main()
{
	
	 int Size_A = 10;
	Animal **A = new Animal*[Size_A];
	// 
	//const int Size_A = 10;
	//Animal* A[Size_A]; //Работает только если Size_A константа

	A[0] = new Doge();
	A[1] =  new Cat();
	A[2] = new Cow();
	A[3] = new Cow();
	A[4] = new Cat();
	A[5] = new Doge();
	A[6] = new  Cat();
	A[7] = new Cow();
	A[8] = new Doge();
	A[9] = new Cow();

	for (int i = 0; i < 10; i++)
	{
		A[i]->Voice();
		delete[] A[i];
	}
	
	delete[] A;
   // std::cout << "Hello World!\n";
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
